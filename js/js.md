### 数据类型

Number String Object Boolean Undefined NUll Symbol BigInt

* Symbol 独一无二不可变数据，解决全局变量冲突问题
* BigInt 大整数，表示任意精度格式的整数，可以超出Number能够表示的范围

### 判断数据类型

* typeof : 数组、对象、null都会判断为object，其它判断都正确
* instanceof: 判断原型链中能否找到该类型的原型，只能判断引用类型数据
* constructor: 通过构造函数判断数据类型,但是constructor属性可以被修改，不是最可靠的判断方式
```js
// 注：如果创建一个对象来改变它的原型，constructor就不能判断数据类型了
function Fn(){}
Fn.prototype = new Array()
var f = new Fn()
console.log(f.constructor === Fn) // false
console.log(f.constructor === Array) // true
```
* Object.prototype.toString.call()：调用原型上的toString方法判断，可以判断大部分数据类型

### 实现instanceof
```js
function myInstanceof(left, right) {
    var proto = Object.getPrototypeOf(left)
    var prototype = right.prototype
    while(true){
        if(!proto) return false
        if(proto === prototype) return true
        proto = Object.getPrototypeOf(proto)
    }
}
```

### call函数实现
```js
// 思路：1、将函数设为对象的属性 2、执行该函数 3、删除该函数
Function.prototype.call2 = function (context) {
    var context = context || window
    context.fn = this
    var args = []
    for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i])
    }
    var result = context.fn(...args) // es6
    // eval('context.fn(' + args +')') // es3
    delete context.fn
    return result
}
```

### apply函数实现
```js
// 和call类似
Function.prototype.apply2 = function (context, arr) {
    var context = context || window
    context.fn = this 
    var result
    if(!arr) {
        context.fn()
    } else {
        var args = []
        for (var i = 0; i < arr.length; i++) {
            args.push(arr[i])
        }
        var result = context.fn(...args) // es6
        // eval('context.fn(' + args +')') // es3
    }
    delete context.fn
    return result
}
```

### bind函数实现
```js
// 注： 当返回的函数作为构造函数使用时，bind指定的值会失效，但传参依然生效
Function.prototype.bind2 = function(context){
    var self = this
    var args = Array.prototype.slice.call(arguments, 1)
      
    var bindFn =  function () {
        var newArgs = Array.prototype.slice.call(arguments)
        return self.apply(this instanceof FNull ? this : context, args.concat(newArgs))
    }
    // bindFn.prototype = self.prototype // 如果直接这么写，bindFn可能会被外部修改
    var FNull = function () {} 
    FNull.prototype = this.prototype
    bindFn.prototype = new FNull()
    return bindFn
}
```

### new操作实现

new 运算符创建一个用户定义的对象类型的实例或具有构造函数的内置对象类型之一

```js
function newObj() {
    // var obj = new Object()
    var obj = Object.create({})
    Constructor = [].shift.call(arguments)
    obj__proto__ = Constructor.prototype
    var res = Constructor.apply(obj, arguments)
    return typeof res === "object" ? (res || obj) : obj
}
```

### 类数组

拥有一个 length 属性和若干索引属性的对象
```js
var arrayLike = {
    0: 'name',
    1: 'age',
    2: 'sex',
    length: 3
}

// 类数组转数组
Array.prototype.slice.call(arrayLike, 0)
Array.prototype.splice.call(arrayLike, 0)
Array.from(arrayLike)
Array.prototype.concat.apply([], arrayLike)
```

### arguments对象

length属性：实参长队
callee属性：函数自身

```js
function foo(name, age, sex, hobbit) {

    console.log(name, arguments[0]); // name name

    // 改变形参
    name = 'new name';

    console.log(name, arguments[0]); // new name new name

    // 改变arguments
    arguments[1] = 'new age';

    console.log(age, arguments[1]); // new age new age

    // 测试未传入的是否会绑定
    console.log(sex); // undefined

    sex = 'new sex';

    console.log(sex, arguments[2]); // new sex undefined

    arguments[3] = 'new hobbit';

    console.log(hobbit, arguments[3]); // undefined new hobbit

}

foo('name', 'age')

// 传入的参数，实参和 arguments 的值会共享，当没有传入时，实参与 arguments 值不会共享
// 除此之外，以上是在非严格模式下，如果是在严格模式下，实参和 arguments 是不会共享的。
```

### JSON.stringify

* 处理基本类型时，与使用toString基本相同，结果都是字符串，除了 undefined
```js
console.log(JSON.stringify(null)) // null
console.log(JSON.stringify(undefined)) // undefined，注意这个undefined不是字符串的undefined
console.log(JSON.stringify(true)) // true
console.log(JSON.stringify(42)) // 42
console.log(JSON.stringify("42")) // "42"
```
* 布尔值、数字、字符串的包装对象在序列化过程中会自动转换成对应的原始值
```js
JSON.stringify([new Number(1), new String("false"), new Boolean(false)]); // "[1,"false",false]"
```
* undefined、任意的函数以及 symbol 值，在序列化过程中会被忽略（出现在非数组对象的属性值中时）或者被转换成 null（出现在数组中时）
```js
JSON.stringify({x: undefined, y: Object, z: Symbol("")}); 
// "{}"

JSON.stringify([undefined, Object, Symbol("")]);          
// "[null,null,null]" 
```
* JSON.stringify 有第二个参数 replacer，它可以是数组或者函数，用来指定对象序列化过程中哪些属性应该被处理，哪些应该被排除
```js
function replacer(key, value) {
  if (typeof value === "string") {
    return undefined;
  }
  return value;
}

var foo = {foundation: "Mozilla", model: "box", week: 45, transport: "car", month: 7};
var jsonString = JSON.stringify(foo, replacer);

console.log(jsonString)
// {"week":45,"month":7}

var foo = {foundation: "Mozilla", model: "box", week: 45, transport: "car", month: 7};
console.log(JSON.stringify(foo, ['week', 'month']));
// {"week":45,"month":7}
```
* 如果一个被序列化的对象拥有 toJSON 方法，那么该 toJSON 方法就会覆盖该对象默认的序列化行为：不是那个对象被序列化，而是调用 toJSON 方法后的返回值会被序列化
```js
var obj = {
  foo: 'foo',
  toJSON: function () {
    return 'bar';
  }
};
JSON.stringify(obj);      // '"bar"'
JSON.stringify({x: obj}); // '{"x":"bar"}'
```

### == 相等

当执行x == y 时：

* 如果x与y是同一类型：
    * x是Undefined，返回true
    * x是Null，返回true
    * x是数字：
        * x是NaN，返回false
        * y是NaN，返回false
        * x与y相等，返回true
        * x是+0，y是-0，返回true
        * x是-0，y是+0，返回true
        * 返回false
    * x是字符串，完全相等返回true,否则返回false
    * x是布尔值，x和y都是true或者false，返回true，否则返回false
    * x和y指向同一个对象，返回true，否则返回false
    * x是null并且y是undefined，返回true

* x是undefined并且y是null，返回true

* x是数字，y是字符串，判断x == ToNumber(y)

* x是字符串，y是数字，判断ToNumber(x) == y

* x是布尔值，判断ToNumber(x) == y

* y是布尔值，判断x ==ToNumber(y)

* x是字符串或者数字，y是对象，判断x == ToPrimitive(y)

* x是对象，y是字符串或者数字，判断ToPrimitive(x) == y

* 返回false

### 